﻿using UnityEngine;
using System.Collections;

public class test : MonoBehaviour {

	public float x,y,z;
	Quaternion q;
	public GameObject cube;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		q = Quaternion.Euler (x, y, z);

		Vector3 eulerAngles = q.eulerAngles;
		eulerAngles.y = 0;
		cube.transform.rotation = Quaternion.Euler (eulerAngles);
	}
}
